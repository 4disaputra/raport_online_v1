<?php
error_reporting(0);
session_start();
if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
include "login.php";
}else{
	include "../config/koneksi.php";
	include "../config/fungsi_indotgl.php";
	include "../config/library.php";
	include "../config/fungsi.php";
	include "lib/lang.php";
	include "statistic.php";

$default = "http://en.gravatar.com/userimage/1419645/b382dec3ac430523336e7a253d22e040.jpg";
$size = 40;
$grav_url = "http://www.gravatar.com/avatar/" .md5( strtolower( trim( $_SESSION['mailuser'] ) ) ) . "?d=" .urlencode( $default ) . "&s=" . $size;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin Panel</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
		<link href="editor/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="editor/css/froala_editor.min.css" rel="stylesheet" type="text/css">
		<link href="editor/css/froala_style.min.css" rel="stylesheet" type="text/css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
				<!-- jQuery 2.0.3 -->
   		<script data-require="jquery@*" data-semver="2.0.3" src="js/jquery-2.0.3.min.js"></script>
		<!-- Bootstrap 3.1.1 -->
		<script data-require="bootstrap@*" data-semver="3.0.3" src="js/bootstrap.min.js"></script>
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.php" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Admin
            </a>
            <!-- Header Navbar: style can be found in header.less -->
          <?php include "menu_atas.php"; ?>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?=$grav_url;?>" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $_SESSION['namalengkap']; ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form --
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Chat with us"/>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
					
                 <?php include "sidebar.php"; ?>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small><?php echo $_GET['module']; ?></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><?php echo $_GET['module']; ?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
				 <div class="row">
   
                    <!-- Main row -->
<?php 
$module = $_GET['module'];
$pathFile = 'module/'.$_GET['module'].'/index.php';
if (file_exists($pathFile))
{
include 'module/'.$_GET['module'].'/index.php';
}else {
?>
                    <div class="error-page">
                        <h2 class="headline text-info"> 404</h2>
                        <div class="error-content">
                            <h3><i class="fa fa-warning text-yellow"></i> Oops! Module <?php echo $_GET['module']; ?> tidak ditemukan.</h3>
                            <p>
                               Kami tidak dapat menemukan halaman yang Anda cari. Sementara itu, Anda dapat kembali ke <a href='index.php'>return to dashboard</a> atau pilih menu lain.
                            </p>
                        </div><!-- /.error-content -->
                    </div><!-- /.error-page -->
	<?php
}
?>
				</div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- Bootstrap WYSIHTML5 -->
        <script src="js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
		<!-- Notifikasi -->
		<script src="js/notifikasi.js" type="text/javascript"></script>
		<!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="js/AdminLTE/dashboard.js" type="text/javascript"></script>        
		        <!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
                $('#example3').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                // CKEDITOR.replace('editor1');
                //bootstrap WYSIHTML5 - text editor
               // $(".textarea").wysihtml5();
            });
        </script> 
  <script src="editor/js/froala_editor.min.js"></script>
  <!--[if lt IE 9]>
    <script src="editor/js/froala_editor_ie8.min.js"></script>
  <![endif]-->
  <script src="editor/js/plugins/tables.min.js"></script>
  <script src="editor/js/plugins/lists.min.js"></script>
  <script src="editor/js/plugins/colors.min.js"></script>
  <script src="editor/js/plugins/file_upload.min.js"></script>
  <script src="editor/js/plugins/media_manager.min.js"></script>
  <script src="editor/js/plugins/font_family.min.js"></script>
  <script src="editor/js/plugins/font_size.min.js"></script>
  <script src="editor/js/plugins/block_styles.min.js"></script>
  <script src="editor/js/plugins/video.min.js"></script>
 <script>
 $(function(){
$("#edit").editable({
inlineMode: false,
imageUpload: true,
mediaManager: true, 
imageUploadParam: 'file',
imageUploadURL: 'http://rks.cu.cc/adminweb/upload_image.php',
imagesLoadURL: "http://rks.cu.cc/adminweb/images_load.php",
height:"200px"
})
});
</script>
    </body>
</html>
<?php 
}
 ?>