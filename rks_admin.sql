-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 04. Desember 2017 jam 08:06
-- Versi Server: 5.1.41
-- Versi PHP: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `rks_admin`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE IF NOT EXISTS `guru` (
  `id_guru` int(25) NOT NULL AUTO_INCREMENT,
  `nip` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `tempat` varchar(255) NOT NULL,
  `tanggal` varchar(255) NOT NULL,
  `jk` varchar(255) NOT NULL,
  `telepon` varchar(255) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id_guru`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`id_guru`, `nip`, `nama`, `alamat`, `tempat`, `tanggal`, `jk`, `telepon`, `jabatan`, `password`) VALUES
(2, 'nip', 'maimunah', 'alamat', 'temp', 'tangga', 'jen', 'tekeon', 'jabant', 'nip'),
(4, '123', 'dedi', 'j', 'j', 'j', 'j', 'j', 'j', '123');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
  `id_kelas` int(15) NOT NULL AUTO_INCREMENT,
  `kelas` varchar(255) NOT NULL,
  `tahun` varchar(255) NOT NULL,
  `semester` varchar(25) NOT NULL,
  PRIMARY KEY (`id_kelas`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `kelas`, `tahun`, `semester`) VALUES
(6, 'X D', '2018', '1'),
(5, 'X D', '2017', '1'),
(3, 'X B', '2017', '1'),
(4, 'X A', '2017', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel`
--

CREATE TABLE IF NOT EXISTS `mapel` (
  `id_mapel` int(11) NOT NULL AUTO_INCREMENT,
  `id_guru` varchar(255) NOT NULL,
  `mapel` varchar(255) NOT NULL,
  `id_kelas` varchar(255) NOT NULL,
  `kkm` varchar(255) NOT NULL,
  PRIMARY KEY (`id_mapel`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data untuk tabel `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `id_guru`, `mapel`, `id_kelas`, `kkm`) VALUES
(2, '2', 'bahasa', '6', '7'),
(4, '4', 'Matematika', '6', '7'),
(5, '4', 'mtk', '3', '7'),
(10, '2', 'Sejarah', '6', '70'),
(11, '2', 'MATIK', '4', '90');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nasabah`
--

CREATE TABLE IF NOT EXISTS `nasabah` (
  `id_nasabah` int(25) NOT NULL AUTO_INCREMENT,
  `no_rekening` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no_telepon` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `tanda_tangan` varchar(50) NOT NULL,
  `foto` varchar(50) NOT NULL,
  PRIMARY KEY (`id_nasabah`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data untuk tabel `nasabah`
--

INSERT INTO `nasabah` (`id_nasabah`, `no_rekening`, `nama`, `no_telepon`, `alamat`, `tanda_tangan`, `foto`) VALUES
(8, '1234567', 'indra', '0999999999', 'alamat', '3.PNG', '4.PNG'),
(7, '12345', 'nama', '09090', 'lamat', 'c4.png', 'c6.png'),
(6, '12346', 'adi', '09090', 'alamat', 'c30.png', 'c29.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilaipts`
--

CREATE TABLE IF NOT EXISTS `nilaipts` (
  `id_nilaipts` int(25) NOT NULL AUTO_INCREMENT,
  `id_siswa` varchar(255) NOT NULL,
  `id_kelas` varchar(255) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `tahun` varchar(255) NOT NULL,
  `harian1` varchar(255) NOT NULL,
  `remedi1` varchar(255) NOT NULL,
  `harian2` varchar(255) NOT NULL,
  `remedi2` varchar(255) NOT NULL,
  `harian3` varchar(255) NOT NULL,
  `remedi3` varchar(255) NOT NULL,
  `harian4` varchar(255) NOT NULL,
  `remedi4` varchar(255) NOT NULL,
  `harian5` varchar(255) NOT NULL,
  `remedi5` varchar(255) NOT NULL,
  `harian6` varchar(255) NOT NULL,
  `remedi6` varchar(255) NOT NULL,
  `ratarata` varchar(255) NOT NULL,
  `tugas1` varchar(255) NOT NULL,
  `tugas2` varchar(255) NOT NULL,
  `tugas3` varchar(255) NOT NULL,
  `tugas4` varchar(255) NOT NULL,
  `tugas5` varchar(255) NOT NULL,
  `tugas6` varchar(255) NOT NULL,
  `rataratatugas` varchar(255) NOT NULL,
  `pts` varchar(255) NOT NULL,
  `pts_remedi` varchar(255) NOT NULL,
  `uas` varchar(25) NOT NULL,
  `uas_remedi` varchar(25) NOT NULL,
  PRIMARY KEY (`id_nilaipts`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data untuk tabel `nilaipts`
--

INSERT INTO `nilaipts` (`id_nilaipts`, `id_siswa`, `id_kelas`, `id_mapel`, `tahun`, `harian1`, `remedi1`, `harian2`, `remedi2`, `harian3`, `remedi3`, `harian4`, `remedi4`, `harian5`, `remedi5`, `harian6`, `remedi6`, `ratarata`, `tugas1`, `tugas2`, `tugas3`, `tugas4`, `tugas5`, `tugas6`, `rataratatugas`, `pts`, `pts_remedi`, `uas`, `uas_remedi`) VALUES
(7, '4', '4', 1, '2017', '8', '9', '8', '7', '8', '9', '8', '9', '8', '9', '8', '9', '8', '8', '9', '8', '9', '8', '8', '8.6666666666667', '7', '8', '8', '6'),
(8, '8', '3', 1, '2017', '8', '8', '8', '8', '8', '8', '8', '7', '7', '7', '7', '7', '7.6666666666667', '7', '7', '7', '8', '8', '8', '7.5', '7', '7', '7', '5'),
(2, '$_POST[''siswa'']', '$kelas', 1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, '$_POST[''siswa'']', '$_POST[''kelas'']', 1, '$_POST[''tahun'']', '$_POST[''nh1'']', '$_POST[''r1'']', '$_POST[''nh2'']', '$_POST[''r2'']', '$_POST[''nh3'']', '$_POST[''r3'']', '$_POST[''nh4'']', '$_POST[''r4'']', '$_POST[''nh5'']', '$_POST[''r5'']', '$_POST[''nh6'']', '$_POST[''r6'']', '$ratarata', '$_POST[''nt1'']', '$_POST[''nt2'']', '$_POST[''nt3'']', '$_POST[''nt4'']', '$_POST[''nt5'']', '$_POST[''nt6'']', '$rataratatugas', '$_POST[''pts'']', '$_POST[''ptsr'']', '$_POST[''uas'']', '$_POST[''uasr'']'),
(10, '9', '6', 2, '', '8', '8', '8', '8', '8', '8', '8', '8', '', '8', '8', '8', '', '8', '8', '8', '8', '8', '8', '', '8', '8', '8', '8'),
(15, '9', '6', 4, '2017', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8'),
(14, '9', '6', 2, '2017', '9', '9', '9', '9', '9', '8', '8', '8', '7', '8', '7', '7', '8.1666666666667', '7', '6', '8', '8', '8', '8', '7.5', '8', '8', '8', '8'),
(16, '9', '6', 10, '2017', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90', '90');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE IF NOT EXISTS `siswa` (
  `id_siswa` int(25) NOT NULL AUTO_INCREMENT,
  `id_kelas` varchar(255) NOT NULL,
  `nis` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `agama` varchar(255) NOT NULL,
  `tahun_ajaran` varchar(255) NOT NULL,
  PRIMARY KEY (`id_siswa`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `id_kelas`, `nis`, `nama`, `alamat`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `agama`, `tahun_ajaran`) VALUES
(8, '3', '124', 'desti', 'l', 'l', 'l', 'l', 'l', '2017'),
(2, '$kelas', '$nis', '$nama', '$alamat', '$tempat', '$tanggal', '$jk', '$agama', '2017'),
(3, 'nis', '', 'nama', 'almat', 'tempat', 'tangga', 'jk', 'k', '2017'),
(6, '4', '1234', 'Dedi', 'Jl', 'palembang', '10-02-2000', 'Laki-Laki', 'islam', '2017'),
(7, '4', '12345', 'Desi', 'l', 'l', 'l', 'l', 'l', '2017'),
(9, '6', '1344', 'ani', 'a', 'k', 'k', 'k', 'k', '2017');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nama_lengkap` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `no_telp` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `level` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT 'user',
  `blokir` enum('Y','N') COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `id_session` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `hits` int(5) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`username`, `password`, `nama_lengkap`, `email`, `no_telp`, `level`, `blokir`, `id_session`, `hits`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'a@gmail.com', '00000', 'admin', 'N', 'hhgmpu72epj1v2kkudc6oqpg44', 48);
