<?php
//Deteksi hanya bisa diinclude, tidak bisa langsung dibuka (direct open)
if(count(get_included_files())==1)
{
	echo "<meta http-equiv='refresh' content='0; url=http://$_SERVER[HTTP_HOST]'>";
	exit("Direct access not permitted.");
}
?>
				   <ul class="sidebar-menu">
                        <li>
                            <a href="index.php">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        
                        	<li class="treeview">
                            <a href="#">
                                <i class="fa fa-book"></i>
                                <span>Data Kelas</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
							<li class="">
                            <a href="media.php?module=kelas">
                                <i class="fa fa-list"></i> <span>List Kelas</span>
                            </a>							
                            </ul>
                        </li>
                        
                        
                        
						<li class="treeview">
                            <a href="#">
                                <i class="fa fa-book"></i>
                                <span>Data Siswa</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
							<li class="">
                            <a href="media.php?module=datasiswa">
                                <i class="fa fa-list"></i> <span>List Data Siswa</span>
                            </a>
							</li>
                            </ul>
                        </li>
						
							<li class="treeview">
                            <a href="#">
                                <i class="fa fa-book"></i>
                                <span>Data Mata Pelajaran</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
							<li class="">
                            <a href="media.php?module=mapel">
                                <i class="fa fa-list"></i> <span>List Mata Pelajaran</span>
                            </a>
							</li>							
                            </ul>
                        </li>
						<li class="treeview">
                            <a href="#">
                                <i class="fa fa-book"></i>
                                <span>Data Guru</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
							<li class="">
                            <a href="media.php?module=dataguru">
                                <i class="fa fa-list"></i> <span>List Data Guru</span>
                            </a>
							</li>							
                            </ul>
                        </li>
						
						
						
							<li class="treeview">
                            <a href="#">
                                <i class="fa fa-book"></i>
                                <span>Data Nilai / Raport</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
							<li class="">
                            <a href="media.php?module=nilaipts">
                                <i class="fa fa-list"></i> <span>List Nilai PTS / Raport</span>
                            </a>
							</li>
							<li class="">
                            <a href="media.php?module=nilaiuas">
                                <i class="fa fa-list"></i> <span>List Nilai UAS/ Raport</span>
                            </a>
							</li>
                            <li class="">
                            <a href="media.php?module=nilairaporpts">
                                <i class="fa fa-list"></i> <span>List Nilai UAS/ Raport </span>
                            </a>
							</li>
						 
							
                            </ul>
                        </li>
					 
                    </ul>