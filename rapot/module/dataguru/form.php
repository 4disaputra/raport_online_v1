<?php
//Deteksi hanya bisa diinclude, tidak bisa langsung dibuka (direct open)
if(count(get_included_files())==1)
{
	echo "<meta http-equiv='refresh' content='0; url=http://$_SERVER[HTTP_HOST]'>";
	exit("Direct access not permitted.");
}

if (empty($_SESSION['namauser']) AND empty($_SESSION['passuser'])){
header('location:../index.php');
}else{

$idberita = mysql_real_escape_string($_GET['id']);
// query untuk menampilkan mahasiswa berdasarkan kd_mesin
$data = mysql_fetch_array(mysql_query("SELECT * FROM berita WHERE id_berita=".$idberita));

// jika kd_mesin > 0 / form ubah data
if($idberita > 0) {
	$judul = $data['judul'];
	$berita = $data['isi_berita'];
	$tanggal = tgl_indo($data[tanggal]);
	
	if($data['headline']=="Y") {
		$cek1 = '<input type="radio" name="headline" value="Y" checked>';
		$cek2 = '<input type="radio" name="headline" value="N">';
	} else{
		$cek1 = '<input type="radio" name="headline" value="Y">';
		$cek2 = '<input type="radio" name="headline" value="N" checked>';
	}
//form tambah data
} else {
	$judul = "";
	$berita = "";
	$tanggal = date("d-m-Y");
	$cek1 = '<input type="radio" name="headline" value="Y" checked>';
	$cek2 = '<input type="radio" name="headline" value="N">';
}

?>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
					<form role="form" method="POST" action="?module=<?=$module;?>&act=save">
					<input type="hidden" name="id" value="<?=$idberita;?>">
                        <div class="col-md-8">
                            <div class="box">
								<div class="box box-primary">
                                <div class="box-header">
								<?php if($idberita > 0) { ?>
                                    <h3 class="box-title">Edit Berita</h3>
									<?php }else{ ?>
                                    <h3 class="box-title">Input Berita</h3>
									<?php } ?>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                
                                <div class="box-body">
								<div class='box-body pad'>
                                        <div class="form-group">
                                            <label>Judul Berita</label>
                                            <input type="text" name="judul" class="form-control" placeholder="Judul Berita" value="<?php echo $judul; ?>">
                                        </div>
								<!-- select -->
                                <div class="form-group">
                                <label>Kategori Berita</label>
								<select name="kategori" class="form-control">
								<?php $tampil=mysql_query("SELECT * FROM kategori where aktif='Y' ORDER BY nama_kategori");
								if ($data['id_kategori']==0){
								echo "<option value=0 selected>- Pilih Kategori -</option>";
								}   
								while($w=mysql_fetch_array($tampil)){
								if ($data['id_kategori']==$w['id_kategori']){
								echo "<option value=$w[id_kategori] selected>$w[nama_kategori]</option>";
								}else{
								echo "<option value=$w[id_kategori]>$w[nama_kategori]</option>";
									}
								}
								?>
								</select>
                                </div>
                                </div>
                                <div class='box-header'>
                                    <h3 class='box-title'>Isi Berita</h3>
                                </div><!-- /.box-header -->
								<section id="editor">
                                <div class='box-body pad'>
                                        <textarea id='edit' name='isi_berita' class="textarea" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $berita; ?></textarea>                      
                                </div>
                                </section>
								</div>

                                </div><!-- /.box-body -->

                                    <div class="box-footer">
									<?php if($idberita > 0) { ?>
                                        <button type="submit" name="update" class="btn btn-primary">Update</button>
									<?php }else{ ?>
                                        <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
									<?php } ?>
										<a href="?module=<?=$module;?>" class="btn btn-danger">Batal</a>
                                    </div>
                            </div><!-- /.box -->
                        </div>
						<!-- kanan -->
						<div class="col-md-4">
                            <div class="box box-solid">
                                <div class="box-body">
                                    <div class="box-group" id="accordion">
                                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                        <div class="panel box box-primary">
                                            <div class="box-header">
                                                <h4 class="box-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                        Info Berita
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in">
                                                <div class="box-body">
                                                    <!-- text input -->
                                        <div class="form-group">
                                            <label>Author</label>
                                            <input type="text" class="form-control" value="<?php echo"$_SESSION[namauser]"; ?>"/>
											<label>Tanggal Publish</label>
                                            <input type="text" class="form-control" value="<?php echo$tanggal; ?>" />
											<label>Gambar</label>
											<input type='file' class="input-long" name='fupload'/>
                                        </div>
                                        <!-- radio -->
                                        <div class="form-group"> 
											<label>Headline</label>
                                            <div class="radio">
                                                <label>
                                                    <?=$cek1;?>Ya
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                  <?=$cek2;?>Tidak
                                                </label>
                                            </div>
                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel box box-danger">
                                            <div class="box-header">
                                                <h4 class="box-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                        TAG Berita
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse">
                                                <div class="box-body">
                                                    <div class="form-group">
													 <label>
								<?php $d = GetCheckboxes('tag', 'tag_seo', 'nama_tag', $data['tag']);
													echo $d; ?> 
													</label>
													</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel box box-success">
                                            <div class="box-header">
                                                <h4 class="box-title">
                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                       Opsi 2
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse">
                                                <div class="box-body">
                                                    <div class="box-body">
                                                    <div class="form-group">
													<label>Text</label>
													<input type="text" class="form-control" value="" placeholder="......"/>
													</div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- /.col -->
					</form>	
                    </div><!-- /.row -->                    

                </section><!-- /.content -->
<?php 
}
 ?>