<?php
$sql = mysql_query("SELECT * FROM guru");
?>

<div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data Guru</h3>     
            <div class="box-footer clearfix no-border">
                <a href="?module=<?= $module; ?>&act=tambah<?= $module; ?>" class="btn btn-default pull-right">
                    <i class="fa fa-plus"></i> Tambah Guru</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example2" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="width:1% !important;" >No</th>
                        <th style="width:10%;text-align:center">NIP</th>
                        <th style="width:10%;text-align:center">Nama Guru</th>
                        <th style="width:20%;text-align:center">Alamat</th>
                        <th style="width:10%;text-align:center">Tempat Lahir</th>
                        <th style="width:10%;text-align:center">Tanggal Lahir</th>
                        <th style="width:5%;text-align:center">Jenis Kelamin</th>
                        <th style="width:10%;text-align:center">Telpon</th>
                        <th style="width:10%;text-align:center">Jabatan</th>
                        <th style="width:5%;text-align:center">Aksi</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $no = 1;
                    while ($row = mysql_fetch_array($sql)) {
                        ?>
                        <tr>
                            <td align="center"><?= $no; ?></td>
                            <td align="left"><?php echo $row['nip']; ?></td>
                            <td><?php echo $row['nama']; ?></td>
                            <td align="left"><?php echo $row['alamat']; ?></td>
                            <td> <?php echo $row['tempat']; ?> </td>
                            <td align="left"><?php echo $row['tanggal']; ?></td>
                            <td><?php echo $row['jk']; ?></td>
                            <td align="left"><?php echo $row['telepon']; ?></td>
                            <td><?php echo $row['jabatan']; ?></td>
                            <td align="center">
                            <a href="?module=<?= $module; ?>&act=edit<?= $module; ?>&id=<?= $row['id_guru']; ?>"  data-toggle="tooltip" title="Edit Data"><i class="fa fa-edit"></i> </a>
                                <a href="?module=<?= $module; ?>&act=hapus&id=<?= $row['id_guru']; ?>"  data-toggle="tooltip" title="Hapus Data"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
<?php } ?>

                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>

