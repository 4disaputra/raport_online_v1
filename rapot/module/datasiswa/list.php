<?php
$sql = mysql_query("SELECT * FROM siswa where id_kelas='$_GET[id_kelas]'");
?>

<div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data Siswa</h3>     
            <div class="box-footer clearfix no-border">
                <a href="?module=<?= $module; ?>&id_kelas=<?= $_GET['id_kelas']; ?>&act=tambah<?= $module; ?>" class="btn btn-default pull-right"><i class="fa fa-plus"></i>
                    
                    Tambah Siswa</a>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body table-responsive">
            <table id="example2" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="width:1% !important;" >No</th>
                        <th style="width:10%;text-align:center">No Induk</th>
                        <th style="width:10%;text-align:center">Nama Siswa</th>
                        <th style="width:20%;text-align:center">Alamat</th>
                        <th style="width:10%;text-align:center">Tempat Lahir</th>
                        <th style="width:10%;text-align:center">Tanggal Lahir</th>
                        <th style="width:5%;text-align:center">Jenis Kelamin</th>
                        <th style="width:10%;text-align:center">Agama</th>

                        <th style="width:5%;text-align:center">Aksi</th>
                    </tr>
                </thead>
                <tbody>


                    <?php $no = 1;
                    while ($row = mysql_fetch_array($sql)) {
                        ?>
                        <tr>
                            <td align="center"><?= $no; ?></td>
                            <td align="left"><?php echo $row['nis']; ?></td>
                            <td><?php echo $row['nama']; ?></td>
                            <td align="left"><?php echo $row['alamat']; ?></td>
                            <td><?php echo $row['tempat_lahir']; ?></td>
                            <td align="left"><?php echo $row['tanggal_lahir']; ?></td>
                            <td><?php echo $row['jenis_kelamin']; ?></td>
                            <td align="left"><?php echo $row['agama']; ?></td>

                            <td align="center">
                                <a href="?module=<?= $module; ?>&act=edit<?= $module; ?>&id=<?= $row['id_siswa']; ?>"  data-toggle="tooltip" title="Edit Data"><i class="fa fa-edit"></i> </a>
                                <a href="?module=<?= $module; ?>&act=hapus&id=<?= $row['id_siswa']; ?>"  title="Hapus Data"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>